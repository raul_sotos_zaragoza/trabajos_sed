library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-------------------------------------------------------------------------------------------
entity FMS_Ascensor is
  port ( 
   ----ENTRADAS----
   clk : in  std_logic;
   reset : in std_logic;
   --boton para abrir la puerta si se esta en reposo
   bap : in std_logic;
   --botones de confirmaci�n de selecci�n de piso y de llamada 
   bcs, bcll : in std_logic;
   --se�al de piso actual decodificada en Control_Switches que viene de los 6 switches de la derecha
   s_piso_act_in : in std_logic_vector(2 downto 0);
   --se�al para simular la llamada desde fuera (decodificada en Control_Switches)
   s_piso_llam : in std_logic_vector(2 downto 0);
   --se�al de piso seleccion (salida de FMS_Seleccion) 
   s_piso_sel_in : in std_logic_vector(2 downto 0);
   ----SALIDAS----
   motor : out std_logic_vector(1 downto 0);
   s_piso_act_out : out std_logic_vector(2 downto 0); --se indica el valor en el ultimo display  
   s_piso_sel_out : out std_logic_vector(2 downto 0); --se indica el valor en el ultimo display
   leds_estado : out std_logic_vector(3 downto 0);
   leds_llamada : out std_logic_vector(5 downto 0)
  );
end FMS_Ascensor;
-------------------------------------------------------------------------------------------
architecture behavioral of FMS_Ascensor is
--se�ales
  type estados_t is (reposo,subiendo,bajando,emergencia);
 signal estado_actual, estado_siguiente : estados_t := reposo;
-- signal s_leds_estado : std_logic_vector(3 downto 0):="0000";
-- signal s_leds_llamada : std_logic_vector(5 downto 0):="000000";
-- signal piso_sig  : std_logic_vector(2 downto 0):= "000";

 --maquina de estado que representa el estado del motor
begin
  registro_estado: process (clk,reset) begin
  --actualiza el estado del ascensor a cada flanco de reloj
    if(rising_edge(clk)) then
    if reset = '0' then
      estado_actual <= reposo;
      --s_leds_llamada <= "000000";
    else
      estado_actual <= estado_siguiente;
    end if;
    end if;
  end process;
  decodificador_estado_motor: process(bcll,bcs,s_piso_sel_in,s_piso_llam,s_piso_act_in,estado_actual)
   variable piso_sig  : std_logic_vector(2 downto 0):= "000";
  begin
  estado_siguiente <= estado_actual;
    case(estado_actual) is
    --REPOSO--
      when reposo =>
       motor <= "00";
       leds_estado <= "0010";
       if(s_piso_act_in < "110") then
       piso_sig := s_piso_act_in;
         if(bcs = '1') then
          piso_sig := s_piso_sel_in;
         elsif(bcll = '1') then
          case s_piso_llam is
            when "000" => leds_llamada <= "100000";
            when "001" => leds_llamada <= "010000";
            when "010" => leds_llamada <= "001000";
            when "011" => leds_llamada <= "000100";
            when "100" => leds_llamada <= "000010";
            when "101" => leds_llamada <= "000001";
            when others => null;          
          end case;
             if(s_piso_llam /= "111") then
              piso_sig := s_piso_llam;
             end if;
--          else
--           leds_llamada <= "000000";
          end if;
          
--         if(bap = '1') then --led de se�al de puerta abierta
--          s_leds_estado <= "0010"; end if;

          --gestion del cambio de piso
            if(piso_sig > s_piso_act_in) then 
              estado_siguiente <= subiendo;
            elsif(piso_sig < s_piso_act_in) then 
              estado_siguiente <= bajando;
            elsif(piso_sig = s_piso_act_in) then
              estado_siguiente <= reposo;
            end if;
       else
         estado_siguiente <= emergencia;
       end if; 
      --SUBIENDO-- 
      when subiendo =>
      motor <= "10";
      leds_estado <= "1000";
      if (s_piso_act_in = piso_sig) then
          estado_siguiente <= reposo;
          leds_llamada <= "000000";
--          leds_estado <= "0000";
      elsif (s_piso_act_in > piso_sig) then
          estado_siguiente <= emergencia;
      end if;
      --BAJANDO--
      when bajando =>
      motor <= "01";
      leds_estado <= "0100";
      if (s_piso_act_in = piso_sig) then
          estado_siguiente <= reposo;
          leds_llamada <= "000000";
--          s_leds_estado <= "0000";
      elsif (s_piso_act_in < piso_sig) then
          estado_siguiente <= emergencia;
      end if;
      when emergencia =>
      motor <= "11";
      leds_estado <= "0001";
        if (s_piso_act_in = piso_sig) then
          estado_siguiente <= reposo;
        end if; 
      when others => null;
    end case;
  end process;
    s_piso_sel_out <= s_piso_sel_in;
    s_piso_act_out <= s_piso_act_in;
end behavioral;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-------------------------------------------------------------------------------------------
entity Control_Botones is  
  port (
  --Entradas 
    clk : in std_logic;
    botones_v5 : in std_logic_vector(4 downto 0);
 --Salidas   
    ss_bsd, ss_bsi, ss_bap, ss_bcs, ss_bcll: out std_logic
  );
end Control_Botones;
-------------------------------------------------------------------------------------------
architecture structural of Control_Botones is
  --componentes
  COMPONENT edgedtctr is
    port ( 
        clk : in std_logic;
        sync_in : in std_logic;
        edge : out std_logic
    ); end COMPONENT;
  COMPONENT synchrnzr is
    port ( 
        clk : in std_logic;
        async_in : in std_logic;
        sync_out : out std_logic
        
    ); end COMPONENT;
     --se�ales botones
      signal s_bsd, s_bsi, s_bap, s_bcs, s_bcll : std_logic := '0';
begin

--Gestion de los botones (sincronizadores y detectores de flanco)   
 i3_1 : synchrnzr port MAP ( 
    --entradas
    clk =>clk,
    async_in => botones_v5(0),
    --salidas
    sync_out => s_bap
    );
    
 i3_2 : edgedtctr port MAP ( 
    --entradas
    clk => clk,
    sync_in => s_bap,
    --salidas
    edge => ss_bap
    );
  i3_3 : synchrnzr port MAP ( 
    --entradas
    clk =>clk,
    async_in=> botones_v5(1),
    --salidas
    sync_out => s_bsd
    );
    
 i3_4 : edgedtctr port MAP ( 
    --entradas
    clk => clk,
    sync_in => s_bsd,
    --salidas
    edge => ss_bsd
    );
 i3_5 : synchrnzr port MAP ( 
    --entradas
    clk =>clk,
    async_in=> botones_v5(2),
    --salidas
    sync_out => s_bsi
    );
    
 i3_6 : edgedtctr port MAP ( 
    --entradas
    clk => clk,
    sync_in => s_bsi,
    --salidas
    edge => ss_bsi
    );
 i3_7 : synchrnzr port MAP ( 
    --entradas
    clk =>clk,
    async_in=> botones_v5(3),
    --salidas
    sync_out => s_bcs
    );
    
 i3_8 : edgedtctr port MAP ( 
    --entradas
    clk => clk,
    sync_in => s_bcs,
    --salidas
    edge => ss_bcs
    );
 i3_9 : synchrnzr port MAP ( 
    --entradas
    clk =>clk,
    async_in=> botones_v5(4),
    --salidas
    sync_out => s_bcll
    );
    
 i3_10 : edgedtctr port MAP ( 
    --entradas
    clk => clk,
    sync_in => s_bcll,
    --salidas
    edge => ss_bcll
    );
    
end structural;

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Ra�l Sotos Zaragoza
-- 
-- Create Date: 17.12.2020 12:41:13
-- Design Name:  
-- Module Name: top - Behavioral
-- Project Name: ControlAscensor6Pisos
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
-----------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-----------------------------ENTIDAD-----------------------------------------------------
ENTITY top IS
    PORT (
--ENTRADAS
   --reloj principal y reset asincrono
    clk, reset : in std_logic;
    botones : in std_logic_vector(4 downto 0);
   --para simular una llamada (desde fuera del ascensor) y cotrolar los sensores de posicion
    sw : in std_logic_vector(15 downto 0);
--SALIDAS
   ----LEDS----
    leds : out std_logic_vector(15 downto 0);
   ----DISPLAYS----
   --Simbolos de piso (0,1,2,3,4,5), Simbolos de motor (sub,baj,-,E) 
    display : out std_logic_vector(6 downto 0);
   --Configuracion del display (se van alternando los 8 con una se�al de reloj de menor frecuencia)
    control_display : out std_logic_vector(7 downto 0)
    );
END top;
----------------------------ARQUITECTURA---------------------------------------------------
ARCHITECTURE structural OF top IS
--Preparacion de la arquitectura (declaraciones)
  --COMPONENTES-- 
   COMPONENT FMS_Ascensor is
    port ( 
      clk : in std_logic;
      reset : in std_logic;
      bcs, bcll, bap : in std_logic;
      s_piso_llam : in std_logic_vector(2 downto 0);
      s_piso_act_in : in std_logic_vector(2 downto 0);
      s_piso_sel_in : in std_logic_vector(2 downto 0);
      motor : out std_logic_vector(1 downto 0);
      s_piso_act_out : out std_logic_vector(2 downto 0); --se indica el valor en el ultimo display  
      s_piso_sel_out : out std_logic_vector(2 downto 0); --se indica el valor en el ultimo display
      leds_estado : out std_logic_vector(3 downto 0);
      leds_llamada : out std_logic_vector(5 downto 0)
      ); end COMPONENT;
   COMPONENT FMS_Seleccion is
    port ( 
      reset : in std_logic;
      clk : in std_logic;
      bsd, bsi : in std_logic;
      piso_sel : out std_logic_vector(2 downto 0)
  ); end COMPONENT;  
   COMPONENT Control_Displays7Seg is
    port ( 
      reset : in std_logic;
      clk : in std_logic;
      piso_act : in std_logic_vector(2 downto 0);
      piso_sel : in std_logic_vector(2 downto 0);
      motor : in std_logic_vector(1 downto 0);
      display : out std_logic_vector(6 downto 0); --configuracion del display
      ctrl_display : out std_logic_vector(7 downto 0) --activacion de 1 de los 8 displays
    ); end COMPONENT;  
   COMPONENT Control_Botones is  
   port (
      --Entradas 
      clk : in std_logic;
      botones_v5: in std_logic_vector(4 downto 0);
      --Salidas   
      ss_bsd, ss_bsi, ss_bap, ss_bcs, ss_bcll: out std_logic   
    );end COMPONENT; 
   COMPONENT Control_Switches is
    port (
    --entradas
      sw_v16 : in std_logic_vector(15 downto 0);
    --salidas 
      s_piso_llam : out std_logic_vector(2 downto 0);
      s_piso_act : out std_logic_vector(2 downto 0)
      ); end COMPONENT; 
   COMPONENT Control_LEDs is
    port (
    --entradas 
      clk : in std_logic;
      reset : in std_logic;
      leds_llam : in std_logic_vector(5 downto 0);
      s_piso_act : in std_logic_vector(2 downto 0);
      leds_estado : in std_logic_vector(3 downto 0);
    --salidas
      leds_v16 : out std_logic_vector(15 downto 0)
      ); end COMPONENT;     
  --SE�ALES--
      signal s_clk : std_logic := '0';
   --se�al motor
      signal s_motor : std_logic_vector(1 downto 0) := "00";      
   --se�ales de los pisos
      signal s_piso_act, ss_piso_act, s_piso_sel, ss_piso_sel, s_piso_llam :  std_logic_vector(2 downto 0) := "000";
   --se�ales botones
      signal s_bap, s_bsd, s_bsi, s_bcll, s_bcs : std_logic := '0';
   --se�ales para los leds   
      signal s_leds_llam :  std_logic_vector(5 downto 0);
      signal s_leds_sen_pos :  std_logic_vector(5 downto 0);
      signal s_leds_estado :  std_logic_vector(3 downto 0);
      
--Inicio de la arquitectura (instanciaciones y conexiones)
  BEGIN
----FSM DEL ASCENSOR DE 6 PISOS----
--tiene como componente otra FMS para la seleccion de piso desde dentro del ascensor que gestiona tambien las llamadas externas 
 i1 : FMS_Ascensor
   PORT MAP(
    --entradas
     clk => clk,
     reset => reset,
     bap => s_bap,
     bcs => s_bcs,
     bcll => s_bcll,
     s_piso_act_in => s_piso_act,
     s_piso_sel_in => s_piso_sel,
     s_piso_llam => s_piso_llam,
    --salidas
     motor => s_motor,
     s_piso_act_out => ss_piso_act,
     s_piso_sel_out => ss_piso_sel,
     leds_estado => s_leds_estado,
     leds_llamada => s_leds_llam 
    );
  i1_1 : FMS_Seleccion 
     PORT MAP(
     --entradas
       clk => clk,
       reset => reset,
       bsd => s_bsd, 
       bsi => s_bsi, 
     --salidas
       piso_sel => s_piso_sel --se�al de piso a seleccionar (se muestra en los 6 primeros displays con 1 activo, el resto con '-')
       );     
----CONTROL DE LOS 8 DISPLAYS DE 7 SEGMENTOS---- 
--los 6 primeros para la seleccion de piso, otro para mostrar el estado del ascensor y otro el n�mero piso actual 
  i2: Control_Displays7Seg 
   PORT MAP ( 
    --entradas
     clk => clk, 
     reset => reset, 
     motor => s_motor,
     piso_act => ss_piso_act, 
     piso_sel => ss_piso_sel,
    --salidas 
     display => display, 
     ctrl_display => control_display 
    );
----CONTROL DE LOS 5 BOTONES---- 
--reset asincrono, 
i3: Control_Botones
   PORT MAP (
    --entradas
     clk => clk,  
     botones_v5 => botones,
    --salidas
     ss_bcs => s_bcs, 
     ss_bcll => s_bcll,
     ss_bsd => s_bsd, 
     ss_bsi => s_bsi, 
     ss_bap => s_bap
    );  
----CONTROL DE LOS SWITCHES---- 
 i4 : Control_Switches PORT MAP (
   --entradas
     sw_v16 => sw,
   --salidas
     s_piso_act => s_piso_act,
     s_piso_llam => s_piso_llam
    );  
----CONTROL DE LOS LEDS----
 i5 : Control_LEDs PORT MAP (
   --entradas
     clk => clk,
     reset => reset,
     leds_estado => s_leds_estado,
     leds_llam => s_leds_llam,
     s_piso_act => ss_piso_act,
   --salidas  
     leds_v16 => leds
    );
END structural;
-------------------------------------------------------------------------------------------
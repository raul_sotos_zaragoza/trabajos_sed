library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-----------------------------------------------------------------------------------------
entity FMS_Seleccion is
  Port ( 
  --entradas
  reset: in std_logic;
  clk : in std_logic;
  bsd, bsi : in std_logic;
  --salidas
  piso_sel : out std_logic_vector(2 downto 0)
  );
end FMS_Seleccion;
-----------------------------------------------------------------------------------------
architecture behavioral of FMS_Seleccion is
   type estado is (S0, S1, S2, S3, S4, S5, S6); --Displays de seleccion
  signal estado_actual: estado := S0;
  signal estado_siguiente: estado := S0;
  signal s_piso_sel  : std_logic_vector(2 downto 0):= "000";
begin
  registro_estado: process (reset, clk)
    begin
        if (rising_edge(clk)) then
            if reset = '0' then
                estado_actual <= S0;
            else
                estado_actual <= estado_siguiente;
            end if;
        end if;
    end process;
  decodificador_estado_seleccion: process (bsd, bsi, s_piso_sel, estado_actual)
    begin
        estado_siguiente <= estado_actual;
        piso_sel <= s_piso_sel;
        case estado_actual is
        when S0 =>
          s_piso_sel <= "000";
            if bsd = '1' then
                estado_siguiente <= S1;
            elsif bsi = '1' then
                estado_siguiente <= S5;
            end if;
        when S1 =>
          s_piso_sel <= "001";
            if bsd = '1' then
                estado_siguiente <= S2;
            elsif bsi = '1' then
                estado_siguiente <= S0;
            end if;
        when S2 =>
          s_piso_sel <= "010";
            if bsd = '1' then
                estado_siguiente <= S3;
            elsif bsi = '1' then
                estado_siguiente <= S1;
            end if;
        when S3 =>
          s_piso_sel <= "011";
            if bsd = '1' then
                estado_siguiente <= S4;
            elsif bsi = '1' then
                estado_siguiente <= S2;
            end if;
        when S4 =>
          s_piso_sel <= "100";
            if bsd = '1' then
                estado_siguiente <= S5;
            elsif bsi = '1' then
                estado_siguiente <= S3;
            end if;
        when S5 =>
          s_piso_sel <= "101";
            if bsd = '1' then
                estado_siguiente <= S0;
            elsif bsi = '1' then
                estado_siguiente <= S4;
            end if;
        when others => null;
        end case;
    end process;
    
end behavioral;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-------------------------------------------------------------------------------------------
entity Control_Displays7Seg is
  Port ( 
  --entradas
  reset : in std_logic;
  clk : in std_logic;
  piso_sel : in std_logic_vector(2 downto 0);
  piso_act : in std_logic_vector(2 downto 0);
  motor : in std_logic_vector(1 downto 0);
  --salidas
  display : out std_logic_vector(6 downto 0);
  ctrl_display : out std_logic_vector(7 downto 0)
  );
end Control_Displays7Seg;
-------------------------------------------------------------------------------------------
architecture behavioral of Control_Displays7Seg is
--Componentes
  component decoder_BCDto7Seg is
    port (
        code_BCD : in std_logic_vector(3 DOWNTO 0);
        leds_7Seg : out std_logic_vector(6 DOWNTO 0)
        ); end component; 
  component clk_divider is
    generic (div: integer := 100000000 );
    port ( 
     clk     : in std_logic;
     reset   : in std_logic;
     clk_out : out std_logic
    ); end component;   
--Se�ales        
  signal s_ctrl_display : std_logic_vector(7 downto 0);
  signal s_code : std_logic_vector(3 downto 0) := "1111";
  signal flag : std_logic_vector(2 downto 0) := "000";
  signal s_clk : std_logic := '0';
 
BEGIN --inicio de la arquiteqtura
  i2_1: clk_divider 
 GENERIC MAP ( 
     div => 80000 --reloj de frecuencia 100MHz/200000 = 1250 Hz
 )
 PORT MAP ( 
     clk => clk, 
     reset => reset, 
     clk_out => s_clk 
 );
PROCESS(reset, s_clk, motor, piso_act, piso_sel) 
 begin
 if rising_edge(s_clk) then
  IF reset = '0' then
       s_ctrl_display <= "11111111";  --todos apagados
       flag <= "000";
  ELSE      
   CASE flag is   
     WHEN "000" =>
       s_ctrl_display <= "01111111";--primer display
          if piso_sel = "000" then s_code <= "0000";-- numero 0
          else s_code <= "1101"; end if;
       flag <= "001";
     WHEN "001" =>
       s_ctrl_display <="10111111";
          if piso_sel = "001" then s_code <= "0001";-- numero 1
          else s_code <= "1101"; end if;      
       flag <= "010";
     WHEN "010" =>
       s_ctrl_display <="11011111"; 
          if piso_sel = "010" then s_code <="0010";-- numero 2
          else s_code <= "1101"; end if;
      s_ctrl_display <="11011111";
       flag <= "011";
     WHEN "011" =>
       s_ctrl_display <= "11101111";
          if piso_sel = "011" then s_code <= "0011";-- numero 3
          else s_code <= "1101"; end if;
       flag <= "100";
     WHEN "100" =>
       s_ctrl_display <= "11110111";
          if piso_sel = "100" then s_code <= "0100";-- numero 4
          else s_code <= "1101"; end if;
       flag <= "101";
     WHEN "101" =>
       s_ctrl_display <= "11111011";
          if piso_sel = "101" then s_code <= "0101";-- numero 5
          else s_code <= "1101"; end if;
       flag <= "110";
     WHEN "110" => ----DISPLAY MOTOR----
       s_ctrl_display <= "11111101";
          if    motor = "10" then s_code <= "1011";--ascensor subiendo
          elsif motor = "01" then s_code <= "1010";--ascensor bajando
          elsif motor = "00" then s_code <= "1111";--ascensor parado 
          elsif motor = "11" then s_code <= "1100"; end if;--emergencia
       flag <= "111";      
     WHEN "111" => ----DISPLAY PISO ACTUAL----
       s_ctrl_display <= "11111110";
        case piso_act is
          when "000" => s_code <= "0000";-- numero 0
          when "001" => s_code <= "0001";-- numero 1
          when "010" => s_code <= "0010";-- numero 2
          when "011" => s_code <= "0011";-- numero 3
          when "100" => s_code <= "0100";-- numero 4
          when "101" => s_code <= "0101";-- numero 5
          when others => s_code <= "1100"; end case;-- se�al emergencia E
       flag <= "000";
     when others => null;
     end CASE;
    end IF;
   end if;
  end process;
  --salida del display activo 
  ctrl_display <= s_ctrl_display;
  --salida del codigo del display 
i2_2: decoder_BCDto7Seg 
 PORT MAP (
     code_BCD => s_code, 
     leds_7Seg => display
 ); 

 end behavioral;
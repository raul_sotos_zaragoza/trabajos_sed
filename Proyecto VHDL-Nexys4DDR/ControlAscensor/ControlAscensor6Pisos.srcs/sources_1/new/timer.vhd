library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity timer is
    generic (
        N: integer := 300
     );
    port (
        clk : in std_logic;
        start,reset : in std_logic;
        fin : out std_logic := '0' 
     );
end timer;

architecture behavioral of timer is
component clk_divider is
 generic (div: integer := 100000000 );
 port ( 
     clk     : in std_logic;
     reset   : in std_logic;
     clk_out : out std_logic
 
 ); end component;  
signal s_clk : std_logic := '0';
begin
  i: clk_divider 
 GENERIC MAP ( 
     div => 1000000 --reloj de frecuencia 100MHz/1000000 = 100 Hz
 )
 PORT MAP ( 
     clk => clk, 
     reset => reset, 
     clk_out => s_clk 
 );
process(s_clk,reset,start)
    subtype count is integer range 0 to (N - 1);
    variable contador : count:=0;
    begin
     if start = '1' then
       if rising_edge(s_clk) then
          if contador < N then
             contador := contador+1;
          elsif start = '1' and contador = N then
             fin <= '1';
          end if;
       end if;
     elsif reset = '0' then
      contador := 0;
      fin <= '0';
     end if;  
    end process;
end behavioral;

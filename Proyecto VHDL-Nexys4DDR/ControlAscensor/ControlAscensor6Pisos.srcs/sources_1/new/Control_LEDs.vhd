library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-------------------------------------------------------------------------------------------
entity Control_LEDs is
   port (
   ----ENTRADAS----
   --RELOJ 100MHz Y RESET ASINCRONO 
   clk : in std_logic;
   reset : in std_logic;
   --SE�ALES QUE LLEGAN PARA LA ACTIVACION DE LOS LEDs
   --se activa uno si se simula una llamada desde un piso (con un �nico switch en alto y confirmando con el boton)
    leds_llam : in std_logic_vector(5 downto 0);
   --se activa uno si se acciona un sensor de posicion y el resto no (con un �nico switch en alto)
    s_piso_act : in std_logic_vector(2 downto 0); 
   --se activa cuando el motor esta subiendo o bajando (apagado cuando esta en espera o emergencia)
    leds_estado : in std_logic_vector(3 downto 0);
   ----SALIDA----
   leds_v16 : out std_logic_vector(15 downto 0) := "0000000000000000"
   );
end Control_LEDs;
-------------------------------------------------------------------------------------------
architecture behavioral of Control_LEDs is
signal leds_piso_act : std_logic_vector(5 downto 0):= "000000"; 
begin
process (reset, clk) is
  begin
  if rising_edge(clk) then
   if reset = '0' then
    leds_v16 <= "0000000000000000";
   else
    leds_v16(15 downto 10) <= leds_llam;
  --los cuatro del medio: 1� subiendo, 2� bajando 3� puerta abierta, 4� emergencia
    leds_v16(9 downto 6) <= leds_estado;
    leds_v16(5 downto 0) <= leds_piso_act;
   end if;
  end if;
end process;
decodificador_piso_act: process(s_piso_act) is
 begin
 case s_piso_act is
          when "000" => leds_piso_act <= "100000";
          when "001" => leds_piso_act <= "010000";
          when "010" => leds_piso_act <= "001000";
          when "011" => leds_piso_act <= "000100";
          when "100" => leds_piso_act <= "000010";
          when "101" => leds_piso_act <= "000001";
          when others => null;
 end case;
 end process;

end behavioral;


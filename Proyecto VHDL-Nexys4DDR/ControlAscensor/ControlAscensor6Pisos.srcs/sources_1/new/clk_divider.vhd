library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.ALL;
use ieee.std_logic_unsigned.ALL;
-------------------------------------------------------------------------------------------
entity clk_divider is
    Generic (
        div: integer:=100000000         -- frec de 100 MHz
        --Valor por defecto para 1 Hz
    );            
    Port ( 
        clk : IN STD_LOGIC;
        reset : IN STD_LOGIC;
        clk_out : OUT STD_LOGIC
    );
end clk_divider;
-------------------------------------------------------------------------------------------
architecture behavioral of clk_divider is
     signal clk_sig: STD_LOGIC := '1';
  begin
   process (reset, clk)
     variable count: integer:=0;
    begin
    if rising_edge(clk) then
     if (reset='0') then
        count := 0;
        clk_sig <= '0';
     else
        if (count = div)   then
            count := 0;
            clk_sig <= not (clk_sig);
        else 
            count := count + 1;
        end if;
     end if;
    end if;
 end process;
clk_out <= clk_sig;

end behavioral;


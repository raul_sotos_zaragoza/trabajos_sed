library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity counter is
    generic (
        --rango maximo 65536
        N: integer := 1000
     );
    port (
        clk : in std_logic;
        CE : in std_logic;
        reset : in std_logic;
        code : out std_logic_vector (15 downto 0)
     );
end counter;

architecture behavioral of counter is
begin
    process(clk, reset,  CE)
    subtype count is integer range 0 to (N - 1);
    variable contador : count:=0;
    begin
        if rising_edge(clk) then
            if reset = '1' then
                contador := 0;
            else
                if CE = '1' and contador < N then
                    contador := contador+1;
                elsif CE = '1' and contador = N then
                    contador := 0;
                end if;
            end if;
        end if;
    code <= std_logic_vector (to_unsigned (contador, code'length));
    end process;
end behavioral;

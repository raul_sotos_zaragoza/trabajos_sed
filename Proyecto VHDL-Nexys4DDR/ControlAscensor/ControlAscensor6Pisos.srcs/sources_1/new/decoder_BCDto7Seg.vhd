LIBRARY IEEE;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;
-------------------------------------------------------------------------------------------
ENTITY decoder_BCDto7Seg IS
   PORT (
     code_BCD : IN std_logic_vector(3 DOWNTO 0);
     leds_7Seg : OUT std_logic_vector(6 DOWNTO 0)
   );
END ENTITY decoder_BCDto7Seg;
-------------------------------------------------------------------------------------------
ARCHITECTURE dataflow OF decoder_BCDto7Seg IS
 BEGIN
   WITH code_BCD SELECT
     leds_7Seg <= "0000001" WHEN "0000", --CERO (0)
                  "1001111" WHEN "0001",
                  "0010010" WHEN "0010",
                  "0000110" WHEN "0011", --TRES (3)
                  "1001100" WHEN "0100",
                  "0100100" WHEN "0101",
                  "0100000" WHEN "0110", --SEIS (6)
                  "0001111" WHEN "0111",
                  "0000000" WHEN "1000", --OCHO (8)
                  "0000100" WHEN "1001", --NUEVE (9)
                  --SIMBOLOS ADICIONALES
                  "0100011" WHEN "1010", --BAJANDO (SIMBOLO)
                  "1010100" WHEN "1011", --SUBIENDO (SIMBOLO)
                  "0110000" WHEN "1100", --EMERGENCIA (E)
                  "1111111" WHEN "1101", --EMERGENCIA (E)
                  "1111110" WHEN others; --ESPERA (-)
END ARCHITECTURE dataflow;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-------------------------------------------------------------------------------------------
entity Control_Switches is
   port ( 
   sw_v16 : in std_logic_vector(15 downto 0); 
   s_piso_llam : out std_logic_vector(2 downto 0);
   s_piso_act : out std_logic_vector(2 downto 0)
   );
end Control_Switches;
-------------------------------------------------------------------------------------------
architecture dataflow of Control_Switches is
begin
   decodificador_piso_llamada: 
       with sw_v16(15 downto 10) select
        s_piso_llam <= "000" when "100000",
                       "001" when "010000",
                       "010" when "001000",
                       "011" when "000100",
                       "100" when "000010",
                       "101" when "000001",
                       "111" when others;
--Decodifica el estado de los 6 ultimos switches en el valor del piso actual
--(simulando que los switches actuan como final de carrera de cada piso) 
   decodificador_piso_actual: 
       with sw_v16(5 downto 0) select
        s_piso_act <= "000" when "111111",
                       "001" when "111110",
                       "010" when "111100",
                       "011" when "111000",
                       "100" when "110000",
                       "101" when "100000",
                       "111" when others; 

end dataflow;

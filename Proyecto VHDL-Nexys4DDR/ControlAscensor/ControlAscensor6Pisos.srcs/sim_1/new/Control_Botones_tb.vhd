library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Control_Botones_tb is

end Control_Botones_tb;

architecture behavioral of Control_Botones_tb is
   COMPONENT Control_Botones is  
   port (
      --Entradas 
      clk : in std_logic;
      botones_v5: in std_logic_vector(4 downto 0);
      --Salidas   
      ss_bsd, ss_bsi, ss_bap, ss_bcs, ss_bcll: out std_logic   
    );end COMPONENT; 
    signal    botones, botones_out : std_logic_vector(4 downto 0) := "00000";
    signal clk : std_logic := '1';
    constant k: time := 10 ns;--periodo para 100MHz
begin
uut: Control_Botones
 port map(
 clk => clk,
 botones_v5 => botones,
 ss_bsd => botones_out(0),
 ss_bsi => botones_out(1),
 ss_bcs => botones_out(2),
 ss_bcll => botones_out(3),
 ss_bap => botones_out(0) 
 );
test_botones: process
    begin
    wait for 3*k;
        botones <= "00000";
    wait for 3*k;
        botones <= "10000";
    wait for 3*k;
        botones <= "01000";
    wait for 3*k;
        botones <= "00100";
    wait for 3*k;
        botones <= "00010";
    wait for 3*k;
        botones <= "00001";
end process;
        
    process
    begin
        wait for 1.5 sec;
        ASSERT FALSE
        REPORT "Simlación finalizada."
        SEVERITY FAILURE;    
    end process;
end behavioral;

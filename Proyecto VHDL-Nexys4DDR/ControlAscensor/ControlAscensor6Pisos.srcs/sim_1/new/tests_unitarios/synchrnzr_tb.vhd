--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   23:10:50 12/11/2019
-- Design Name:   
-- Module Name:   C:/Users/lcastedo/Documents/etsidi/docencia/Plan2010/grado/sed/2019-2020/pruebas/event_cntr/synchrnzr_tb.vhd
-- Project Name:  event_cntr
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: SYNCHRNZR
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
 
entity synchrnzr_tb is
end synchrnzr_tb;
 
ARCHITECTURE behavioral OF synchrnzr_tb IS 

  -- Component Declaration for the Unit Under Test (UUT)
  component synchrnzr
    port (
      RESET_N  : in  std_logic;
      CLK      : in  std_logic;
      ASYNC_IN : in  std_logic;
      SYNC_OUT : out std_logic
    );
  end component;

  --Inputs
  signal reset_n  : std_logic;
  signal clk      : std_logic;
  signal async_in : std_logic;

  --Outputs
  signal sync_out : std_logic;

  -- Clock period definitions
  constant clk_period : time := 10 ns;
 
begin

  -- Instantiate the Unit Under Test (UUT)
  uut: synchrnzr 
    port map (
      RESET_N  => reset_n,
      CLK      => clk,
      ASYNC_IN => async_in,
      SYNC_OUT => sync_out
    );

  -- Clock process definitions
  clk_process: process
  begin
    clk <= '0';
    wait for 0.5 * clk_period;
    clk <= '1';
    wait for 0.5 * clk_period;
  end process;

  reset_n <= '0' after 0.25 * clk_period, '1' after 0.75 * clk_period;
  
  -- Stimulus process
  stim_proc: process
  begin		
    async_in <= '0';
    wait until reset_n = '1';
    for i in 1 to 3 loop
      wait until clk = '1';
    end loop;
    async_in <= '1' after 0.25 * clk_period;
    for i in 1 to 3 loop
      wait until clk = '1';
    end loop;
    async_in <= '0' after 0.25 * clk_period;
    for i in 1 to 6 loop
      wait until clk = '1';
    end loop;

    wait for 0.25 * clk_period;
    assert false
      report "[SUCCESS]: simulation finished."
      severity failure;
  end process;

end behavioral;

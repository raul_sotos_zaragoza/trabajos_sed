library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.ALL;

ENTITY decoder_BCDto7Seg_tb IS
END decoder_BCDto7Seg_tb;

ARCHITECTURE behavioral OF decoder_BCDto7Seg_tb IS
   COMPONENT decoder_BCDto7Seg
    PORT(
     code_BCD  : IN std_logic_vector(3 DOWNTO 0);
     leds_7Seg : OUT std_logic_vector(6 DOWNTO 0)
    ); END COMPONENT;
    
    SIGNAL code : std_logic_vector(3 DOWNTO 0);
    SIGNAL leds : std_logic_vector(6 DOWNTO 0);

    TYPE vtest is record
     code : std_logic_vector(3 DOWNTO 0);
     leds : std_logic_vector(6 DOWNTO 0);
    END RECORD;
    TYPE vtest_vector IS ARRAY (natural RANGE <>) OF vtest;
    CONSTANT test: vtest_vector := (
       (code => "0000", leds => "0000001"),
       (code => "0001", leds => "1001111"),
       (code => "0010", leds => "0010010"),
       (code => "0011", leds => "0000110"),
       (code => "0100", leds => "1001100"),
       (code => "0101", leds => "0100100"),
       (code => "0110", leds => "0100000"),
       (code => "0111", leds => "0001111"),
       (code => "1000", leds => "0000000"),
       (code => "1001", leds => "0000100"),
       (code => "1010", leds => "1111110"),
       (code => "1011", leds => "1111110"),
       (code => "1100", leds => "1111110"),
       (code => "1101", leds => "1111110"),
       (code => "1110", leds => "1111110"),
       (code => "1111", leds => "1111110")
       );
    BEGIN
     uut: decoder_BCDto7Seg PORT MAP(
       code_BCD => code,
       leds_7Seg => leds
       );
       
     tb: PROCESS
       BEGIN
        FOR i IN 0 TO test'HIGH LOOP
           code <= test(i).code;
           WAIT FOR 20 ns;
         
           ASSERT leds = test(i).leds
            REPORT "Salida incorrecta."
           SEVERITY FAILURE;
        END LOOP;
        
        ASSERT false
        REPORT "Simulacin finalizada. Test superado."
        SEVERITY FAILURE;
     END PROCESS;
END behavioral;

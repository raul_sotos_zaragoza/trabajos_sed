LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;

entity Control_LEDs_tb is
end entity;

architecture behavioral of Control_LEDs_tb is
--Creacion del componente
component Control_LEDs
	port(
     clk : in std_logic;
     reset : in std_logic;
     led_v16 : in std_logic_vector(15 downto 0);
     leds_sel : out std_logic_vector(5 downto 0);
     leds_sen_pos : out std_logic_vector(5 downto 0);
   	 led_motor : out std_logic;
     led_puerta : out std_logic
    );END COMPONENT;

	--Inputs
  	signal reset     : std_logic :='1';
  	signal clk       : std_logic := '0';
  	signal led_v16 : std_logic_vector(15 downto 0) := "0000000000000000";
     --Outputs
    signal leds_sel : std_logic_vector(5 downto 0) := "000000";
    signal leds_sen_pos : std_logic_vector(5 downto 0) := "000000" ;
   	signal led_motor : std_logic := '0';
    signal led_puerta : std_logic := '0';
    
TYPE vtest is record
     led_v16 : std_logic_vector(15 DOWNTO 0);
     leds_sel : std_logic_vector(5 DOWNTO 0);
     leds_sen_pos : std_logic_vector(5 DOWNTO 0);
     led_motor : std_logic;
     led_puerta : std_logic;
     
    END RECORD;
    TYPE vtest_vector IS ARRAY (natural RANGE <>) OF vtest;
    CONSTANT test: vtest_vector := (
       (led_v16 => "0000000000000000", leds_sel => "000000", leds_sen_pos=> "000000", led_motor=> '0', led_puerta=> '0'),
       (led_v16 => "1000000000000001", leds_sel => "100000", leds_sen_pos=> "000001", led_motor=> '0', led_puerta=> '0'),
       (led_v16 => "0100000000000010", leds_sel => "010000", leds_sen_pos=> "000010", led_motor=> '0', led_puerta=> '0'),
       (led_v16 => "0010000000000100", leds_sel => "001000", leds_sen_pos=> "000100", led_motor=> '0', led_puerta=> '0'),
       (led_v16 => "0001000000001000", leds_sel => "000100", leds_sen_pos=> "001000", led_motor=> '0', led_puerta=> '0'),
       (led_v16 => "0000100000010000", leds_sel => "000010", leds_sen_pos=> "010000", led_motor=> '0', led_puerta=> '0'),
       (led_v16 => "0000010000100000", leds_sel => "000001", leds_sen_pos=> "100000", led_motor=> '0', led_puerta=> '0'),
       (led_v16 => "0000001001000000", leds_sel => "000000", leds_sen_pos=> "000000", led_motor=> '0', led_puerta=> '0'),
       (led_v16 => "0000000110000000", leds_sel => "000000", leds_sen_pos=> "000000", led_motor=> '0', led_puerta=> '0'),
       (led_v16 => "0000000000000000", leds_sel => "111111", leds_sen_pos=> "111111", led_motor=> '0', led_puerta=> '0')
       );

	begin 
	-- Creaci�n de componente
    uut: Control_LEDs
    port map (
        clk           => clk,
        reset         => reset,
        led_v16       => led_v16,
        leds_sel      => leds_sel,
        leds_sen_pos  => leds_sen_pos,
        led_motor     => led_motor,
        led_puerta    => led_puerta
    );


tb: PROCESS
   BEGIN
     FOR i IN 0 TO test'HIGH LOOP
         led_v16 <= test(i).led_v16;
           WAIT FOR 20 ns;
         
           ASSERT (leds_sel = test(i).leds_sel, leds_sen_pos = test(i).leds_sen_pos, led_motor = test(i).led_motor,led_puerta = test(i).led_puerta);
            REPORT "Salida incorrecta."
           SEVERITY FAILURE;
        END LOOP;
        
        ASSERT false
        REPORT "Simulacin finalizada. Test superado."
        SEVERITY FAILURE;
     END PROCESS;
END behavioral;
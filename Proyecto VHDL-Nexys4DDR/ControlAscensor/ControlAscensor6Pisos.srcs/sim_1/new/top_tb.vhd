----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Ra�l Sotos Zaragoza
-- 
-- Create Date: 17.12.2020 12:41:13
-- Design Name:  
-- Module Name: top_tb - Behavioral
-- Project Name: ControlAscensor6Pisos
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
-----------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-----------------------------------------------------------------------------------------
entity top_tb is
end top_tb;
-----------------------------------------------------------------------------------------
architecture Behavioral of top_tb is
component top is
    PORT (
    clk, reset : in std_logic;
    botones : in std_logic_vector(4 downto 0);
    sw : in std_logic_vector(15 downto 0);
    leds : out std_logic_vector(15 downto 0);
    display : out std_logic_vector(6 downto 0);
    control_display : out std_logic_vector(7 downto 0)
    );
end component;

signal clk, reset : std_logic := '1';
signal    botones : std_logic_vector(4 downto 0) := "00000";
signal    sw : std_logic_vector(15 downto 0) := "0000001111111111";
signal    leds : std_logic_vector(15 downto 0) := "0000000000000000";
signal    display : std_logic_vector(6 downto 0);
signal    control_display : std_logic_vector(7 downto 0);
    constant kk: time := 10 ns;--periodo para 100MHz
    constant k: time := 1 ms;--periodo 1kHz
--verificacion del test
  TYPE vtest_botones is record
    reset : std_logic;
    botones : std_logic_vector(4 downto 0);
    leds : std_logic_vector(15 downto 0);
    display : std_logic_vector(6 downto 0);
    control_display : std_logic_vector(7 downto 0);
  END RECORD;
--  --pendiente
--    TYPE vtest_vector IS ARRAY (natural RANGE <>) OF vtest_botones;
--    CONSTANT test: vtest_vector := (
--       (reset => '0', botones => "00000", leds => "0000000000000000", display => "0000000", control_display => "11111111"),
--       (reset => '1', botones => "10000", leds => "0000000000000000", display => "0000000", control_display => "11111111")
--       );
begin

uut: top
 port map(
 clk => clk,
 reset => reset,
 botones => botones,
 sw => sw,
 leds => leds,
 display => display,
 control_display => control_display
 );
 clk<=not clk after 0.5*kk;
test_botones: process
    begin
    wait for 3*k;
        sw(15 downto 10) <= "000001";--switches llamada piso 5
    wait for 3*k;
        botones <= "10000";--boton confirmar llamada
    wait for 3*k;
        sw(5 downto 0) <= "100000";--piso actual 5
    --cambio de selecion con bsd
    wait for 3*k;
        botones <= "01000";
    wait for 3*k;
        botones <= "01000";
    --cambio de seleccion con bsi
    wait for 3*k;
        botones <= "00100";
    wait for 3*k;
        botones <= "00100";         
    --boton confirmar seleccion
    wait for 3*k;
        botones <= "00010";
    wait for 3*k;
        botones <= "00001"; --boton abrir puerta
    wait for 3*k;
end process;
    process
    begin
        wait for 1.5 sec;
        ASSERT FALSE
        REPORT "Simlaci�n finalizada."
        SEVERITY FAILURE;    
    end process;
end Behavioral;
